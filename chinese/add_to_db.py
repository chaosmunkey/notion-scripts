#!/usr/bin/env python3


# STDLIB IMPORTS
from argparse import ArgumentParser
from calendar import monthrange
from datetime import date
from os import environ

# THIRD PARTY IMPORTS
from requests import post
from yaml import safe_load


NOTION_URL = "https://api.notion.com/v1/pages/"
NOTION_KEY = environ.get("NOTION_SEC")

COURSE_DB = "fe8da400-1fda-421f-a759-7a76f7dff6c1"
LESSON_DB = "4d816a77-320b-4a88-96f0-a5726f1a89a5"


headers = {"Content-type": "application/json", "Authorization": f"Bearer {NOTION_KEY}", "Notion-Version": "2021-08-16"}


def add_course(course):
    """
    Add a new course to the courses database.
    """
    data = {
        "parent": {"type": "database_id", "database_id": COURSE_DB},
        "properties": {
            "Name": {
                "type": "title",
                "title": [
                    {
                        "text": {"content": course.get("courseName")},
                    }
                ],
            },
            "Description": {
                "type": "rich_text",
                "rich_text": [
                    {
                        "type": "text",
                        "text": {
                            "content": course.get("courseDescription"),
                        },
                        "plain_text": course.get("courseDescription"),
                    }
                ],
            },
            "Course #": {"type": "number", "number": course.get("courseNumber")},
            "URL": {"type": "url", "url": course.get("courseURL")},
            "Status (Temp)": {
                "type": "select",
                "select": {
                    "id": "0c94ae26-dc80-4754-941f-36485a286c02",
                },
            },
        },
    }

    req = post(url=NOTION_URL, headers=headers, json=data)
    return req.json().get("id")


def add_lessons(course, parent: str):
    """
    Add the lessons.
    """

    for chapter in range(course.get("numChapters")):

        data = {
            "parent": {"type": "database_id", "database_id": LESSON_DB},
            "properties": {
                "Lesson Name": {
                    "id": "title",
                    "type": "title",
                    "title": [
                        {
                            "type": "text",
                            "text": {"content": f"{course.get('courseName')} - Chapter {chapter + 1}"},
                        }
                    ],
                },
                "Course": {
                    "id": "EPcw",
                    "type": "relation",
                    "relation": [{"id": parent}],
                },
                "Lesson #": {"type": "number", "number": course.get("lessonStart") + chapter},
                "Level": {
                    "type": "select",
                    "select": {"name": course.get("courseLevel")},
                },
                "Link": {"type": "url", "url": f"{course.get('courseURL')}?chapter={chapter + 1}"},
            },
        }
        req = post(url=NOTION_URL, headers=headers, json=data)
        print(req.json().get("id"))


if __name__ == "__main__":
    with open(file="new.yaml", mode="r") as yh:
        courses = safe_load(yh)

    for course in courses:
        print(course)
        parent = add_course(course=course)
        add_lessons(course=course, parent=parent)

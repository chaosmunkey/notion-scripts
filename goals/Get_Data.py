#!/usr/bin/env python3

"""
Retrieve data from a Notion database so that it can
be used to create a graph; Notion doesn't have the
ability to create graphs from data.
"""

# STDLIB IMPORTS
from os import environ

# from json import dump

# THIRD PARTY IMPORTS
from requests import post


DB_QUERY = "https://api.notion.com/v1/databases/{database}/query"
NOTION_DB = "3c310495-3bec-44e2-9c1d-7bc8044d20e0"
NOTION_KEY = environ.get("NOTION_SEC")

headers = {"Content-type": "application/json", "Authorization": f"Bearer {NOTION_KEY}", "Notion-Version": "2021-08-16"}

data = {
    "filter": {
        "and": [
            {"property": "Completed Units", "number": {"greater_than": 0}},
            {
                "or": [
                    {"property": "Goal", "relation": {"contains": "bfdf8eda-110c-41a9-8976-5855b7f80567"}},
                    {"property": "Goal", "relation": {"contains": "6dfa444e-2171-4d2c-b540-92fbc0811529"}},
                ],
            },
        ]
    },
    "sorts": [{"property": "Due Date", "direction": "ascending"}],
}

req = post(url=DB_QUERY.format(database=NOTION_DB), headers=headers, json=data)
# with open(file="out.json", mode="w") as fh:
#     dump(req.json().get("results"), fh, indent=4)

filtered = {}

for item in req.json().get("results"):
    properties = item.get("properties", {})
    date = properties.get("Due Date", {}).get("date", {}).get("start", {})

    if not filtered.get(date):
        filtered[date] = {}
    completed = properties.get("Completed Units", {}).get("number", {})

    name = properties.get("Name", {}).get("title", [])[0].get("text").get("content")

    if "仰卧起坐" in name:
        filtered[date]["situps"] = completed
    elif "俯卧撑" in name:
        filtered[date]["pushups"] = completed


sit_total = 0
push_total = 0

with open(file="csv.csv", mode="w") as csv:
    csv.write("Date,Push-Ups,Total Push-Ups,Sit-Ups,Total Sit-Ups\n")
    for key, value in filtered.items():

        pushup = value.get("pushups", 0)
        situp = value.get("situps", 0)

        push_total += int(pushup)
        sit_total += int(situp)

        csv.write(f"{key},{pushup},{push_total},{situp},{sit_total}\n")

#!/usr/bin/env python3


# STDLIB IMPORTS
from argparse import ArgumentParser
from calendar import monthrange
from datetime import date
from os import environ

# THIRD PARTY IMPORTS
from requests import post


def _get_cli_args():
    """
    Return a Dictionary of arguments passed in from
    the command line.
    """
    _date = date.today()
    _month = _date.month
    _year = _date.year

    _parser = ArgumentParser()
    _parser.add_argument("-m", "--month", type=int, default=_month)
    _parser.add_argument("-y", "--year", type=int, default=_year)

    _args = _parser.parse_args()

    return {"year": _args.year, "month": _args.month}


NUM_TO_CHIN = [
    "零",
    "一",
    "二",
    "三",
    "四",
    "五",
    "六",
    "七",
    "八",
    "九",
    "十",
    "十一",
    "十二",
    "十三",
    "十四",
    "十五",
    "十六",
    "十七",
    "十八",
    "十九",
    "二十",
    "二十一",
    "二十二",
    "二十三",
    "二十四",
    "二十五",
    "二十六",
    "二十七",
    "二十八",
    "二十九",
    "三十",
    "三十一",
]

NOTION_DB = "3c310495-3bec-44e2-9c1d-7bc8044d20e0"
NOTION_URL = "https://api.notion.com/v1/pages/"
NOTION_KEY = environ.get("NOTION_SEC")


headers = {"Content-type": "application/json", "Authorization": f"Bearer {NOTION_KEY}", "Notion-Version": "2021-08-16"}

tasks = [
    {
        "name": "Push Ups",
        "task": "{month}月{day}号 - 俯卧撑",
        "relation": "bfdf8eda-110c-41a9-8976-5855b7f80567",
        "tags": ["85de90d7-28a3-42ee-9da5-15e2d7e71fea", "8f9b3f9e-c18d-4275-89a1-5b83fcc47e8e"],
        "target": 10,
    },
    {
        "name": "Sit Ups",
        "task": "{month}月{day}号 - 仰卧起坐",
        "relation": "6dfa444e-2171-4d2c-b540-92fbc0811529",
        "tags": ["85de90d7-28a3-42ee-9da5-15e2d7e71fea", "8f9b3f9e-c18d-4275-89a1-5b83fcc47e8e"],
        "target": 10,
    },
    {
        "name": "Read Chinese Book",
        "task": "{month}月{day}号 - 读中文书",
        "relation": "f29f5c2f-291a-48fe-b717-ccbad00832f3",
        "tags": ["b9501482-0120-420d-8132-02ef4fbef63f", "8f9b3f9e-c18d-4275-89a1-5b83fcc47e8e"],
        "target": 1,
    },
]


def create_new_entries(month: int, year: int):
    """
    Create the new events.
    """
    for day in range(1, monthrange(year=year, month=month)[1] + 1):

        for task in tasks:

            data = {
                "parent": {"type": "database_id", "database_id": f"{NOTION_DB}"},
                "properties": {
                    "Name": {
                        "type": "title",
                        "title": [
                            {
                                "type": "text",
                                "text": {
                                    "content": task.get("task").format(month=NUM_TO_CHIN[month], day=NUM_TO_CHIN[day])
                                },
                            }
                        ],
                    },
                    "Due Date": {"type": "date", "date": {"start": f"2022-{month:02}-{day:02}"}},
                    "Completed Units": {"type": "number", "number": 0},
                    "Target Units": {"type": "number", "number": task.get("target", 0)},
                    "Goal": {
                        "type": "relation",
                        "relation": [
                            {
                                "id": task.get("relation"),
                            }
                        ],
                    },
                    "Tags": {"type": "multi_select", "multi_select": []},
                },
            }

            if task.get("tags"):
                for tag in task.get("tags"):
                    data["properties"]["Tags"]["multi_select"].append({"id": tag})

            req = post(url=f"{NOTION_URL}", headers=headers, json=data)
            print(req.json())


if __name__ == "__main__":
    args = _get_cli_args()
    create_new_entries(month=args.get("month"), year=args.get("year"))
